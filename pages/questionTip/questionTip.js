// pages/questionTip/questionTip.js
import {getQuestionInfo,addQuestion} from '../../request/store'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qrCodeText: '请扫描二维码',
    selectValue: '',
    questionList: [{
        text: "请选择"
      },
      {
        text: "黑店"
      }
    ],
    descriptionText: '',
    textLength: 0,
    files: [],
    userReceivedImgList: [],
    selectValue:0,
    params:{
      questionEntity:{
        lon:'',
        lat:''
      },
      filePaths:[]
    },
  },
  getAddress(e){
    this.data.params.questionEntity.address=e.detail.value
  },
  changeValue(e){
    // console.log()
    this.setData({
      selectValue:Number(e.detail.value)
    })
    this.data.params.questionEntity.description = this.data.questionList[this.data.selectValue].description

    // console.log()
  },
  getQuestionInfolist(){
    getQuestionInfo({count:10,start1:0}).then(res=>{
      let arr = res.data.results.unshift({description:'请选择'})
      console.log(res.data.results)
      this.setData({
        questionList:res.data.results
      })
    })
  },
  handleScanQRcode: function () {
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ['qrCode'],
      success: res => {
        this.setData({
          qrCodeText: res.result
        })
      }
    })
  },
  bindTextAreaBlur(e) {
    // console.log(e.detail.value.length)
    if (e.detail.value.length == 501) {
      wx.showToast({
        title: '最多只能输入500字',
      })
    } else {
      this.setData({
        textLength: e.detail.value.length
      })
      this.data.params.questionEntity.content = e.detail.value
    }

  },
  handleDeleteImg: function (params) {
    let d = params.currentTarget.dataset.index
    let list = this.data.files
    let receiveList = this.data.userReceivedImgList
    list.splice(d, 1)
    receiveList.splice(d, 1)
    this.setData({
      files: list
    })
    this.setData({
      userReceivedImgList: receiveList
    })
  },
  chooseImage() {
    var that = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log(res)
        if (res.tempFilePaths.length + that.data.files.length > 3) {
          wx.showModal({
            title: '警告',
            content: "最多只能上传三张图片",
            showCancel: false
          })
        } else {
          console.log(res)
          let tempFilePaths = res.tempFilePaths
          let temp = that.data.files
          tempFilePaths.forEach(item => {
            temp.push(item)
            that.setData({
              files: [...temp]
            })
            wx.uploadFile({
              filePath: item,
              name: 'file',
              url: `https://ocn.bearhunting.cn/public/api/seaweed/v1/attachment/upload`,
              header: {
                "Content-Type": "multipart/form-data",
              },
              success: function (res) {
                console.log(res)
                if (res.statusCode == 200) {
                  let d = JSON.parse(res.data).result
                  let temp = that.data.userReceivedImgList
                  temp.push(d[0])
                  that.setData({
                    userReceivedImgList: temp
                  })
                } else {
                  wx.showModal({
                    title: '警告',
                    content: "上传失败，请重试",
                    showCancel: false
                  })
                  let temp = that.data.files
                  temp.pop()
                  that.setData({
                    files: [...temp]
                  })
                }
              }
            })
          })

        }

        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        // that.setData({
        //   files: that.data.files.concat(res.tempFilePaths)
        // });
      }
    })
  },
  submitquestion(){
    this.data.params.questionEntity.lat = wx.getStorageSync('lat')
    this.data.params.questionEntity.lon= wx.getStorageSync('lng')
    this.data.params.filePaths = this.data.userReceivedImgList
    addQuestion(this.data.params).then(res=>{
      if(res.data.error){
        wx.showToast({
          title: '投诉失败，请稍后重试',
          icon:'none'
        })
      }else{
        wx.showToast({
          title: '投诉成功',
        })
        setTimeout(()=>{
          wx.reLaunch({
            url: '/pages/index/index',
          })
        },1500)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getQuestionInfolist()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})