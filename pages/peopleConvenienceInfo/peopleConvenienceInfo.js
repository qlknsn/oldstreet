// pages/travelNotice/travelNotice.js
import { getConvenient } from '../../request/store'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noticeList: [{
      title: '禁止游泳',
      dateTime: '2021/02/26'
    }, {
      title: '禁止捕捉',
      dateTime: '2021/05/13'
    }, {
      title: '禁止乱设摊位',
      dateTime: '2021/05/16'
    }, {
      title: '禁止乱停乱放',
      dateTime: '2021/08/18'
    }]
  },
  getConvenients(){
    getConvenient({
      start:0,
      count:500
    }).then(res=>{
      this.setData({
        noticeList:res.data.results
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getConvenients()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handleClickItem: function (e) {
    console.log(e)
    let d = e.currentTarget.dataset.obj
    wx.navigateTo({
      url: "/pages/peopleConvenienceDetail/peopleConvenienceDetail?id="+d.id
    })
  }
})