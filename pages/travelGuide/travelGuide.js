// pages/travelGuide/travelGuide.js
import {getScenicAll} from '../../request/store'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    IMG_HOST:'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    taskCardList: [{
        title: '庞松洲住宅',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.85km',
      },
      {
        title: '邵家楼',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.95km',
      },
      {
        title: '星月智汇湾',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '00:00~18:00',
        distance: '1.95km',
      },
    ]
  },
  getScenicAlls(){
    getScenicAll({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
    }).then(res=>{
      this.setData({
        taskCardList:res.data.results
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getScenicAlls()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handleRedirect2Detail: function (e) {
    let d = e.currentTarget.dataset.obj;
    console.log(d);
    wx.navigateTo({
      url: '/pages/visitInfoDetail/visitInfoDetail?id=' + d.id + '&lat=' + d.lat + "&lon=" + d.lon + "&type="+d.type,
    })
  },
})