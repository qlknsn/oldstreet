// pages/visitInfoDetail/visitInfoDetail.js
import {
  getVisitInfoDetail,
  getSnackOne,
  getRestaurantOne
} from "../../request/store";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    IMG_HOST: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    filePaths: [],
    visitInfoDetail: {},
    induceTxt: '',
    boxList: [{
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, ]
  },
  goto(e) {
    console.log(e.currentTarget.dataset.item)
    let d = e.currentTarget.dataset.item
    wx.openLocation({
      latitude: d.lat,
      longitude: d.lon,
    })
  },
  handleNavigateBack: function () {
    wx.navigateBack({
      delta: 1,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.type < 4) {
      getVisitInfoDetail(options).then(res => {
        console.log(res)
        this.setData({
          filePaths: res.data.result.filePaths
        })
        if (options.type == 1) {
          this.setData({
            visitInfoDetail: res.data.result.oldStreetEntity
          })
          this.setData({
            induceTxt: '景点'
          })
        } else if (options.type == 2) {
          // cultureEntity
          this.setData({
            visitInfoDetail: res.data.result.cultureEntity
          })
          this.setData({
            induceTxt: '场馆'
          })
        } else if (options.type == 3) {
          // cultureEntity
          this.setData({
            visitInfoDetail: res.data.result.personInfoEntity
          })
          this.setData({
            induceTxt: '人物'
          })
        }

      })
    } else if (options.type == 5) {
      getRestaurantOne(options).then(res => {
        this.setData({
          filePaths: res.data.result.filePaths
        })
        this.setData({
          visitInfoDetail: res.data.result.restaurantEntity
        })

        this.setData({
          induceTxt: '餐饮'
        })

      })
    } else if (options.type == 4) {
      getSnackOne(options).then(res => {
        this.setData({
          filePaths: res.data.result.filePaths
        })
        this.setData({
          visitInfoDetail: res.data.result.snackEntity
        })
        this.setData({
          induceTxt: '小吃'
        })
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})