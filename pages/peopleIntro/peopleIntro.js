// pages/wenbao/wenbao.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    taskCardList: [{
        title: '庞松洲住宅',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.85km',
      },
      {
        title: '邵家楼',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.95km',
      },
      {
        title: '星月智汇湾',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '00:00~18:00',
        distance: '1.95km',
      },
    ]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  handleRedirect2Detail: function (e) {
    let d = e.currentTarget.dataset.obj.url;
    console.log(d);
    wx.navigateTo({
      url: '/pages/peopleIntroDetail/peopleIntroDetail',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})