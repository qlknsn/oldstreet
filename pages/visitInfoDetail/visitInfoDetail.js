// pages/visitInfoDetail/visitInfoDetail.js
import {
  getVisitInfoDetail
} from "../../request/store";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    visitInfoDetail: '',
    filePaths:[],
    IMG_HOST:'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    boxList: [{
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, {
      url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2061%402x.png'

    }, ]
  },

  handleNavigateBack: function () {
    wx.navigateBack({
      delta: 1,
    })
  },
  goto(e) {
    console.log(e.currentTarget.dataset.item)
    let d = e.currentTarget.dataset.item
    wx.openLocation({
      latitude:d.lat,
      longitude:d.lon,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    getVisitInfoDetail(options).then(res => {
      console.log(res)
      this.setData({
        filePaths: res.data.result.filePaths
      })
      if (options.type == 1) {
        this.setData({
          visitInfoDetail: res.data.result.oldStreetEntity
        })
      } else if (options.type == 2) {
        // cultureEntity
        this.setData({
          visitInfoDetail: res.data.result.cultureEntity
        })
      } else if (options.type == 3) {
        // cultureEntity
        this.setData({
          visitInfoDetail: res.data.result.personInfoEntity
        })
      }

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})