// pages/wenbao/wenbao.js
import {
  getVisitInfo,
  getScenicSnack,
  getScenicRestaurant
} from '../../request/store';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    IMG_HOST:'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    taskCardList: [{
        title: '庞松洲住宅',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.85km',
      },
      {
        title: '邵家楼',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '10:00~18:00',
        distance: '1.95km',
      },
      {
        title: '星月智汇湾',
        url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%BB%84%2060%402x%20%281%29.png',
        openRange: '00:00~18:00',
        distance: '1.95km',
      },
    ]
  },
  getVisitInfos(type){
    getVisitInfo({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
      type:type
    }).then(res=>{
      this.setData({
        taskCardList:res.data.results
      })
    })
  },
  getScenicSnacks(){
    getScenicSnack({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
    }).then(res=>{
      this.setData({
        taskCardList:res.data.results
      })
    })
  },
  getScenicRestaurants(){
    getScenicRestaurant({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
    }).then(res=>{
      this.setData({
        taskCardList:res.data.results
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    switch(options.type){
      case '1':
        wx.setNavigationBarTitle({
          title:'老街文保'
        })
        this.getVisitInfos(options.type)
        break;
      case '2':
          wx.setNavigationBarTitle({
            title:'文化场馆'
          })
          this.getVisitInfos(options.type)
          break;
      case '3':
          wx.setNavigationBarTitle({
            title:'人物介绍'
          })
          this.getVisitInfos(options.type)
          break;
      case '4':
          wx.setNavigationBarTitle({
            title:'特色小吃'
          })
          this.getScenicSnacks()
          break;
      case '5':
          wx.setNavigationBarTitle({
            title:'餐饮信息'
          })
          this.getScenicRestaurants()
          break;
    }
  },
  handleRedirect2Detail: function (e) {
    let d = e.currentTarget.dataset.obj;
    console.log(d);
    wx.navigateTo({
      url: '/pages/wenbaoDetail/wenbaoDetail?id=' + d.id + '&lat=' + wx.getStorageSync('lat') + "&lon=" + wx.getStorageSync('lng') + "&type="+d.type,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})