// index.js
// 获取应用实例
const app = getApp();
import {
  getOpenID,
  getWeather
} from "../../request/store";
Page({
  data: {
    startWeather: '',
    endWeather: '',
    titleIconListObj: [{
        text: "游览须知",
        url: "https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E6%B8%B8%E8%A7%88%E9%A1%BB%E7%9F%A5%402x.png",
      },
      {
        text: "参观信息",
        url: "https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E5%8F%82%E8%A7%82%E4%BF%A1%E6%81%AF%402x.png",
      },
      {
        text: "交通信息",
        url: "https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E4%BA%A4%E9%80%9A%402x.png",
      },
      {
        text: "游览导图",
        url: "https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E5%AF%BC%E5%9B%BE%402x.png",
      },
      {
        text: "下拉更多",
        url: "https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E4%B8%8B%E6%8B%89%402x.png",
      },
    ],
  },
  handleRedirect2Question: function () {
    wx.navigateTo({
      url: "/pages/questionTip/questionTip",
    });
  },
  handleShowMore(e) {
    let text = e.currentTarget.dataset.obj.text;
    switch (text) {
      case "下拉更多":
        wx.navigateTo({
          url: "/pages/more/more",
        });
        break;
      case "游览须知":
        wx.navigateTo({
          url: "/pages/travelNotice/travelNotice",
        });
        break;
      case "参观信息":
        wx.navigateTo({
          url: "/pages/visitInfo/visitInfo",
        });
        break;
      case "游览导图":
        wx.navigateTo({
          url: "/pages/travelGuide/travelGuide",
        });
        break;
      case "老街文保":
        wx.navigateTo({
          url: "/pages/wenbao/wenbao",
        });
        break;
      case "文化场馆":
        wx.navigateTo({
          url: "/pages/cultureStadium/cultureStadium",
        });
        break;
      case "交通信息":
        wx.navigateTo({
          url: '/pages/travelInfo/travelInfo',
        })
        break;
      default:
        break;
    }
  },
  handleRedirect2ConvenienceInfo() {
    wx.navigateTo({
      url: "/pages/peopleConvenienceInfo/peopleConvenienceInfo",
    });
  },
  onLoad() {
    getWeather().then((res) => {
      let d = res.data.result;
      this.setData({
        startWeather: d.lowTem,
        endWeather: d.highTem
      })
    });
    wx.login({
      success: (res) => {
        console.log(`code: ${res.code}`);
        getOpenID({
          jsCode: res.code,
        }).then((ress) => {
          console.log(ress.data.result.openid);
          wx.setStorageSync('openid', ress.data.result.openid)
        });
      },
    });
  },
});