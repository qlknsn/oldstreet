// pages/visitInfo/visitInfo.js
import {
  getVisitInfo
} from '../../request/store';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    IMG_HOST:'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    categoryList: [{
      isActive: true,
      text: '全部',
      type:0
    }, {
      isActive: false,
      text: '老街文保',
      type:1
    }, {
      isActive: false,
      text: '文化场馆',
      type:2
    }, {
      isActive: false,
      text: '人物介绍',
      type:3
    }],
    taskCardList: []
  },
  handleRedirect2Detail: function (e) {
    let d = e.currentTarget.dataset.obj;
    console.log(d);
    wx.navigateTo({
      url: '/pages/visitInfoDetail/visitInfoDetail?id=' + d.id + '&lat=' + wx.getStorageSync('lat') + "&lon=" + wx.getStorageSync('lng') + "&type="+d.type,
    })
  },
  checkStatu(e){
    console.log(e.currentTarget.dataset.type)
    this.data.categoryList.forEach((item,index)=>{
      console.log(index)
      if(index==e.currentTarget.dataset.type){
        item.isActive=true
      }else{
        item.isActive = false
      }
    })
    this.setData({
      categoryList:this.data.categoryList
    })
    getVisitInfo({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      // longitude: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
      type:e.currentTarget.dataset.type
    }).then(res => {
      this.setData({
        taskCardList: res.data.results
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    getVisitInfo({
      count: 500,
      start: 0,
      lon: wx.getStorageSync('lng'),
      // longitude: wx.getStorageSync('lng'),
      lat: wx.getStorageSync('lat'),
    }).then(res => {
      this.setData({
        taskCardList: res.data.results
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})