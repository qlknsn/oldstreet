// pages/more/more.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    moreList: [{
        title: "旅游指南",
        children: [{
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E9%A1%BB%E7%9F%A5%402x.png',
            text: '游览须知'
          }, {
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E5%8F%82%E8%A7%82%402x.png',
            text: '参观信息'
          },
          {
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E4%BA%A4%E9%80%9A%402x%E7%9A%84%E5%89%AF%E6%9C%AC.png',
            text: '交通信息'
          },
          {
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E5%AF%BC%E5%9B%BE%402x%E7%9A%84%E5%89%AF%E6%9C%AC.png',
            text: '游览导图'
          },
          {
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E6%96%87%E6%98%8E%E5%85%AC%E7%BA%A6%402x.png',
            text: '文明公约'
          },
        ]
      },
      {
        title: '老街文化',
        children: [{
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E8%80%81%E8%A1%97%E6%96%87%E4%BF%9D%402x.png',
          text: '老街文保',
          type:1
        }, {
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E6%96%87%E5%8C%96%E5%9C%BA%E9%A6%86%402x.png',
          text: '文化场馆',
          type:2
        }, {
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E4%BA%BA%E7%89%A9%E4%BB%8B%E7%BB%8D%402x.png',
          text: '人物介绍',
          type:3
        }, {
          url: '',
          text: ''
        }, {
          url: '',
          text: ''
        }]
      },
      {
        title: '老街餐饮',
        children: [{
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E7%89%B9%E8%89%B2%E5%B0%8F%E5%90%83%402x.png',
            text: '特色小吃',
            type:4
          },
          {
            url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E9%A4%90%E9%A5%AE%E4%BF%A1%E6%81%AF%402x.png',
            text: '餐饮信息',
            type:5
          }, {
            url: '',
            text: ''
          }, {
            url: '',
            text: ''
          }, {
            url: '',
            text: ''
          }
        ]
      },
      {
        title: '便民生活',
        children: [{
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E4%BE%BF%E6%B0%91%E4%BF%A1%E6%81%AF%402x%E7%9A%84%E5%89%AF%E6%9C%AC.png',
          text: '便民信息'
        }, {
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E9%97%AE%E9%A2%98%E6%8A%95%E8%AF%89%402x.png',
          text: '问题投诉'
        }, {
          url: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/sanlinoldstreet/%E5%81%9C%E8%BD%A6%E7%82%B9%E4%BD%8D%402x.png',
          text: '停车点位'
        }, {
          url: '',
          text: ''
        }, {
          url: '',
          text: ''
        }]
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
   

  },
  handleClickItem: function (e) {
    let text = e.currentTarget.dataset.obj.text
    switch (text) {
      case '游览须知':
        wx.navigateTo({
          url: '/pages/travelNotice/travelNotice',
        })
        break;
      case '参观信息':
        wx.navigateTo({
          url: '/pages/visitInfo/visitInfo',
        })
        break;
      case '文明公约':
        wx.navigateTo({
          url: '/pages/cultureRule/cultureRule',
        })
        break;
      case '游览导图':
        wx.navigateTo({
          url: '/pages/travelGuide/travelGuide',
        });
        break;
      case '老街文保':
        wx.navigateTo({
          url: '/pages/wenbao/wenbao?type='+e.currentTarget.dataset.obj.type,
        });
        break;
      case '文化场馆':
        // wx.navigateTo({
        //   url: '/pages/cultureStadium/cultureStadium',
        // });
        wx.navigateTo({
          url: '/pages/wenbao/wenbao?type='+e.currentTarget.dataset.obj.type,
        });
        break;
      case '人物介绍':
        // wx.navigateTo({
        //   url: '/pages/peopleIntro/peopleIntro',
        // });
        wx.navigateTo({
          url: '/pages/wenbao/wenbao?type='+e.currentTarget.dataset.obj.type,
        });
        break;
      case '特色小吃':
        // wx.navigateTo({
        //   url: '/pages/specialDish/specialDish',
        // })
        wx.navigateTo({
          url: '/pages/wenbao/wenbao?type='+e.currentTarget.dataset.obj.type,
        });
        break;
      case '餐饮信息':
        // wx.navigateTo({
        //   url: '/pages/restaurant/restaurant',
        // })
        wx.navigateTo({
          url: '/pages/wenbao/wenbao?type='+e.currentTarget.dataset.obj.type,
        });
        break;
      case '问题投诉':
        wx.navigateTo({
          url: '/pages/questionTip/questionTip',
        })
        break;
      case '停车点位':
        wx.navigateTo({
          url: '/pages/parking/parking',
        })
        break;
      case '便民信息':
        wx.navigateTo({
          url: '/pages/peopleConvenienceInfo/peopleConvenienceInfo',
        })
        break;
      case '交通信息':
       wx.navigateTo({
         url: '/pages/travelInfo/travelInfo',
       })
        break;
      default:
        break;
    }
  }
})