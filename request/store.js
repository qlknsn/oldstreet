import request from "./request";

export function getOpenID(params) {
  return request({
    url: "/public/api/seaweed/v1/userOldStreet/jscode2session",
    data: params,
    method: "get",
  });
}

export function getTravelNotice(params) {
  return request({
    url: "/public/api/oldStreet/v1/tourNotes/getTourNotes",
    data: params,
    method: "get",
  });
}

export function getTravelNoticeDetail(params) {
  return request({
    url: "/public/api/oldStreet/v1/tourNotes/getTourNotesOne",
    data: params,
    method: 'get'
  })
}

export function getWeather(params) {
  return request({
    url: "/public/api/seaweed/v1/userOldStreet/getWeather",
    data: params,
    method: "get",
  });
}

export function getVisitInfo(params) {
  return request({
    url: '/public/api/oldStreet/v1/scenic/getScenicN',
    data: params,
    method: 'get'
  })
}


export function getCultureRule(params) {
  return request({
    url: '/public/api/oldStreet/v1/civilization/getCivilization',
    data: params,
    method: 'get'
  })
}

export function getVisitInfoDetail(params) {
  return request({
    url: "/public/api/oldStreet/v1/scenic/getScenicOne",
    data: params,
    method: 'get',
  })
}
// 问题描述下拉框
export function getQuestionInfo(params) {
  return request({
    url: "/public/api/oldStreet/v1/questionInfo/getQuestionInfo",
    data: params,
    method: 'get',
  })
}
// 问题投诉
export function addQuestion(params) {
  return request({
    url: "/public/api/oldStreet/v1/question/addQuestion",
    data: params,
    method: 'post',
  })
}
// 游览导图
export function getScenicAll(params) {
  return request({
    url: "/public/api/oldStreet/v1/scenic/getScenicAll",
    data: params,
    method: 'get',
  })
}
// 便民信息
export function getConvenient(params) {
  return request({
    url: "/public/api/oldStreet/v1/convenient/getConvenient",
    data: params,
    method: 'get',
  })
}
// 便民信息详情
export function getConvenientOne(params) {
  return request({
    url: "/public/api/oldStreet/v1/convenient/getConvenientOne",
    data: params,
    method: 'get',
  })
}
// 特色小吃
export function getScenicSnack(params) {
  return request({
    url: "/public/api/oldStreet/v1/snack/getScenicSnack",
    data: params,
    method: 'get',
  })
}
// 餐饮信息
export function getScenicRestaurant(params) {
  return request({
    url: "/public/api/oldStreet/v1/restaurant/getScenicRestaurant",
    data: params,
    method: 'get',
  })
}
// 特色小吃详情
export function getSnackOne(params) {
  return request({
    url: "/public/api/oldStreet/v1/snack/getSnackOne",
    data: params,
    method: 'get',
  })
}
// 餐饮信息详情
export function getRestaurantOne(params) {
  return request({
    url: "/public/api/oldStreet/v1/restaurant/getRestaurantOne",
    data: params,
    method: 'get',
  })
}