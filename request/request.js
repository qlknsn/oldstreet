import {
  baseUrl
} from './publicData'
export default function reqeust(params) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseUrl + params.url,
      method: params.method || 'get',
      data: params.data || {},
      header: params.header,
      success: ress => {
        if (ress.data.error == null) {
          resolve(ress)
        } else {
          wx.showToast({
            title: '接口报错',
            icon: 'error'
          })
        }
      },
      fail: err => {
        console.log(err)
        wx.showToast({
          title: '出错啦',
        })
      }
    })
  })
}