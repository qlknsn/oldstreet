// app.js
App({
  onLaunch() {

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    wx.startLocationUpdate({
      success: res => {
        // console.log(res)
      },
      fail: err => {
        // console.log(err)
      }
    })
    wx.onLocationChange(function (e) {
      console.log(e)
      wx.setStorageSync('lng', e.longitude)
      wx.setStorageSync('lat', e.latitude)
    })
  },
  globalData: {
    userInfo: null
  }
})